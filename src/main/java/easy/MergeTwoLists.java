package easy;

/**
 * 描述：合并两个有序列表
 *
 * @author zhanghaoyu
 * @date 2018/12/25 15:31
 **/
public class MergeTwoLists {
    public static class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
    }

    /**
     * 实际上等同于归并排序的链表版
     * @param l1 第一个有序列表
     * @param l2 第二个有序列表
     * @return 返回新的有效list
     */
    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode listNode = new ListNode(0);
        //保存头指针
        ListNode headNode=listNode;
        while(l1!=null&&l2!=null){
            if(l1.val<=l2.val){
                //每次将下一个节点的地址放进新的链表中
                listNode.next=l1;
                l1=l1.next;
                listNode=listNode.next;
            }else{
                listNode.next=l2;
                l2=l2.next;
                listNode=listNode.next;
            }
        }
        if(l1==null){
            listNode.next=l2;
        }
        if(l2==null){
            listNode.next=l1;
        }
        return headNode.next;
    }

    public static void main(String[] args) {
        //通知镇listNode1
        ListNode listNode1 = new ListNode(1);
        ListNode listNode2 = new ListNode(2);
        ListNode listNode3 = new ListNode(4);
        listNode1.next=listNode2;
        listNode2.next=listNode3;

        //头指针listNode4
        ListNode listNode4 = new ListNode(1);
        ListNode listNode5 = new ListNode(3);
        ListNode listNode6 = new ListNode(4);
        listNode4.next=listNode5;
        listNode5.next=listNode6;
        ListNode listNode = mergeTwoLists(listNode1, listNode4);
        System.out.println(listNode);
    }
}
