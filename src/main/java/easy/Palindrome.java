package easy;

/**
 * 描述：判断一个整数是否是回文数
 *
 * @author zhanghaoyu
 * @date 2018/12/24 16:59
 **/
public class Palindrome {
    /**
     * 判断回文数的一般解法，但是此解法可能会导致sum值比x大很多超出int.max的情况
     * @param x
     * @return
     */
    public static boolean isPalindrome(int x) {
        int tempX=x;
        if(tempX>=0){
            int sum=0;
            while (tempX!=0){
                int temp=tempX%10;
                sum=sum*10+temp;
                tempX/=10;
            }
            if(x==sum){
                return true;
            }
        }
        else{
            return false;
        }
        return false;
    }

    /**
     * 优化解法，只判断回文数的前面一半，如果是奇数位的需要除10去掉多余的以为如12312时的12和123
     * @param x
     * @return
     */
    public static boolean isPalindromeTwo(int x){
        int tempX=x;
        if(x<0){
            return false;
        }
        int sum=0;
        while (tempX>sum){
            sum=sum*10+tempX%10;
            tempX/=10;
        }
        return tempX==sum||tempX==sum/10;
    }

    public static void main(String[] args) {
        System.out.println(isPalindromeTwo(121));
        //负数不算是回文数
        System.out.println(isPalindromeTwo(-121));
    }
}
