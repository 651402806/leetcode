package easy;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 描述：有效的括号
 *
 * @author zhanghaoyu
 * @date 2018/12/25 14:33
 **/
public class ValidParentheses {
    public static boolean isValid(String s) {
        Map<Character, Character> map = new HashMap<>(16);
        map.put(')','(');
        map.put(']','[');
        map.put('}','{');
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            //如果是闭符号
            if(map.containsKey(s.charAt(i))){
                char c = stack.isEmpty() ? '#' : stack.pop();
                if(c!=map.get(s.charAt(i))){
                    return false;
                }
            }
            else{
                stack.push(s.charAt(i));
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        System.out.println(isValid("([)"));
    }
}
