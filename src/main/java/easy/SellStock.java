package easy;

/**
 * 描述：给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
 * url：https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock/
 * 如果你最多只允许完成一笔交易（即买入和卖出一支股票），设计一个算法来计算你所能获取的最大利润
 * 买入必须在卖出之前，即数组的序号买必须小于卖
 *
 * @author zhanghaoyu
 * @date 2018/12/26 16:57
 **/
public class SellStock {
    //--------------------暴力求解法----------------281ms
    /**
     * 计算某一天买入后在某一天卖出所得到的最大利润(实际上就是求数组中两个数的最大差值)
     * 时间复杂度O(n^2)循环了n(n+1)/2次
     * 空间复杂度O(1)
     * @param prices 股票价格数组
     * @return 最大利润值
     */
    public static int maxProfit(int[] prices) {
        int max=0;
        for (int i = 0; i < prices.length; i++) {
            //买入股票价格
            int buyPrice = prices[i];
            for (int j = i+1; j < prices.length; j++) {
                int currentProfit=0;
                if(prices[j]>=buyPrice){
                    currentProfit=prices[j]-buyPrice;
                }
                if(max<currentProfit){
                    max=currentProfit;
                }
            }
        }
        return max;
    }

    //-------------一次遍历法-------------------------2ms
    /**
     * 一次遍历找到当前最低的值之后的最大的值，最低值是不断变化的，需要不断更新最小值
     * 但是需要注意并不一定是最后一个最小值和最后一个最大值之差才是最大的利润
     * 循环过程中不断保存最小值，在剩余的便利过程中用当前prices[i]减去这个最小值，然后判断这个利润值是不是最大的
     * 也就是说找到所有的当前最小值，然后在碰到下一个最小值之前使用两个最小值之间的数字，减去前一个最小值
     * 时间复杂度O(n)，只有一个循环
     * 空间复杂度O(1) 不需要大量的存储空间
     * @param prices 股票价格数组
     * @return 最大利润值
     */
    public static int maxProfitTwo(int[] prices) {
        if(prices==null||prices.length==0){
            return 0;
        }
        int max=0;
        int min=Integer.MAX_VALUE;
        for (int i = 0; i < prices.length; i++) {
            //如果当前值是最小值，则保存它
            if(prices[i]<min){
                min=prices[i];
            }else{
                //如果没有是最小值，计算它与最小值之间的利润,并取它与之前保存的最大值之间最大的数
                max=Math.max(prices[i] - min,max);
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] ints = {7, 1, 5, 3, 6, 4};
        int i = maxProfitTwo(ints);
        System.out.println(i);
    }
}
