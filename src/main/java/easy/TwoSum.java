package easy;

import java.util.HashMap;

/**
 * 描述：
 *
 * @author zhanghaoyu
 * @date 2018/12/24 16:33
 **/
public class TwoSum {
    /**
     * 暴力解法虽然简单，但是并不是最优的解法，一遍哈希表法最优
     * @param nums 数值数组
     * @param target 目标值
     * @return 返回和等于目标值的两个整数，且每种输入只有一个答案
     */
    public static int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; i++) {
            if(map.containsKey(target-nums[i])){
                return new int[]{i,map.get(target-nums[i])};
            }
            //如果map.put放在if前那么3,3,target=6时会返回0，0
            map.put(nums[i],i);
        }
        return null;
    }
    public static void main(String[] args) {
        int[] ints = {2, 7, 11, 15};
        int[] ints1 = twoSum(ints, 9);
        for (int i : ints1) {
            System.out.println(i);
        }
    }
}
