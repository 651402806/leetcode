package easy;

/**
 * 描述：删除链表中的节点,使其可以删除某个链表中给定的（非末尾）节点，你将只被给定要求被删除的节点。
 * url:https://leetcode-cn.com/problems/delete-node-in-a-linked-list/
 * @author zhanghaoyu
 * @date 2018/12/27 15:30
 **/
public class DeleteNode {
    public static class ListNode {
      int val;
      ListNode next;
      ListNode(int x) { val = x; }
    }

    /**
     * 删除链表中的节点
     * @param node 传入的要删除的节点
     */
    public static void deleteNode(ListNode node) {
        node.val=node.next.val;
        node.next=node.next.next;
    }

    public static void main(String[] args) {
        ListNode listNode1 = new ListNode(4);
        ListNode listNode2 = new ListNode(5);
        ListNode listNode3 = new ListNode(1);
        ListNode listNode4 = new ListNode(9);
        listNode1.next=listNode2;
        listNode2.next=listNode3;
        listNode3.next=listNode4;
        deleteNode(listNode2);
        while(listNode1!=null){
            System.out.println(listNode1.val);
            listNode1=listNode1.next;
        }
    }
}
