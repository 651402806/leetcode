package easy;

/**
 * 描述：给定一个整数数组nums，找到一个具有最大和的连续子数组，返回其最大和
 *
 * @author zhanghaoyu
 * @date 2018/12/25 20:06
 **/
public class MaximumSubarray {
    //------------------------暴力解法-------------------------
    /**
     * 找到给定数组的一个具有连续最大和的连续子数组复杂度O(n^2)
     * @param nums 给定数组
     * @return 连续子数组的和的最大值
     */
    public static int maxSubArray(int[] nums) {
        if(nums==null||nums.length==0){
            return 0;
        }
        int maxSum=nums[0];
        for (int i = 0; i < nums.length; i++) {
            //sum遍历过程中存储当前的连续子数组的和
            int sum=0;
            for (int j = i; j <nums.length ; j++) {
                sum+=nums[j];
                if(sum>maxSum){
                    maxSum=sum;
                }
            }
        }
        return maxSum;
    }
    //--------------------------动态规划法(优化版)--------------------
    /**
     * 找到给定数组的一个具有连续最大和的连续子数组复杂度O(n)
     * 算是max(num[n],sum[n-1]+nums[n])
     * 算法思路:假设前n-1个数中已经求得最大连续子序列，只需要知道包不包含最后一个
     * num[0]
     * num[1]
     * num[1]+num[2]
     * num[3]
     * num[3]+num[4]
     * num[3]+num[4]+num[5]
     * num[3]+num[4]+num[5]+num[6] 6
     * num[3]+num[4]+num[5]+num[6]+num[7] 1
     * num[3]+num[4]+num[5]+num[6]+num[7]+num[8] 5
     * @param nums 给定数组
     * @return 连续子数组的和的最大值
     */
    public static int maxSubArrayTwo(int[] nums) {
        if(nums==null||nums.length==0){
            return 0;
        }
        int sum=nums[0];
        int max=nums[0];
        for (int i = 1; i < nums.length; i++) {
            //如果前面的连续和为负，加上我会导致和变小，前面的就不要了只留我自己
            if(sum<0){
                sum=nums[i];
            }else{
                sum+=nums[i];
            }
            max=Math.max(max,sum);
        }
        return max;
    }
    //------------------------动态规划(原装版)----------------------------------
    /**
     * 找到给定数组的一个具有连续最大和的连续子数组复杂度O(n)
     * 算是max(num[n],sum[n-1]+nums[n])
     * 算法思路:假设前n-1个数中已经求得最大连续子序列，只需要知道包不包含最后一个
     * num[0]
     * num[1]
     * num[1]+num[2]
     * num[3]
     * num[3]+num[4]
     * num[3]+num[4]+num[5]
     * num[3]+num[4]+num[5]+num[6] 6
     * num[3]+num[4]+num[5]+num[6]+num[7] 1
     * num[3]+num[4]+num[5]+num[6]+num[7]+num[8] 5
     * @param nums 给定数组
     * @return 连续子数组的和的最大值
     */
    public static int maxSubArrayThree(int[] nums) {
        if(nums==null||nums.length==0){
            return 0;
        }
        int[] sum=new int[nums.length];
        sum[0]=nums[0];
        int max=nums[0];
        for (int i = 1; i < nums.length; i++) {
            sum[i]=Math.max(sum[i-1]+nums[i],nums[i]);
            if(sum[i]>max){
                max=sum[i];
            }
        }
        return max;
    }

    public static void main(String[] args) {
        int[] ints = new int[]{-2,1,-3,4,-1,2,1,-5,4};
//        int i = maxSubArray(ints);
        int i = maxSubArrayThree(ints);
        System.out.println(i);
    }
}
