package easy;

/**
 * 描述：从一个排好序的数组中移除重复的元素
 * 返回非重复元素个数，输出数组的非重复元素
 * @author zhanghaoyu
 * @date 2018/12/25 16:24
 **/
public class RemoveDuplicate {
    /**
     * 双指针法i为快指针，j为慢指针
     * @param nums 排好序的数组[1,1,1,2,2,2,4,4,4]
     * @return 除去重复的数值之后的元素个数
     */
    public static int removeDuplicates(int[] nums) {
        if(nums==null||nums.length==0){
            return 0;
        }
        int num = nums[0];
        int j=1;
        for (int i = 1; i < nums.length; i++) {
            //当元素不等于num时说明已经找到了下个非重复元素
            if(num!=nums[i]){
                nums[j]= nums[i];
                num=nums[i];
                j++;
            }
        }
        return j;
    }

    public static void main(String[] args) {
        int[] nums=new int[]{1,1,1,2,2,2,3,3,3,4,4,4};
        int num = removeDuplicates(nums);
        System.out.println(num);
        for (int i = 0; i < num; i++) {
            System.out.println(nums[i]);
        }
    }
}
