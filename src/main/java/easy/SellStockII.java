package easy;

/**
 * 描述：某一天购买股票后，在之后某一天卖出，然后可以在买进卖出，但是不能同时买进两支同样的股票
 * url:https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/
 * @author zhanghaoyu
 * @date 2018/12/27 11:09
 **/
public class SellStockII {
    //-----------------峰谷法--------------2ms
    /**
     * 只要降价买入，在提价的前一天卖出将其总和加起来，就会大于最大值减去最小值
     * @param prices 随天数变化的股票价格
     * @return 最大利润
     */
    public static int maxProfit(int[] prices) {
        if(prices==null||prices.length==0){
            return 0;
        }
        int currentMax=prices[0];
        int currentMin=prices[0];
        int profit=0;
        for (int i = 0; i < prices.length-1; i++) {
            //后一天价格比前一天低，降价了可以买入,但是要找到最后一个低谷对应的值，在走向低谷的路上的值不能购买
            while(i < prices.length-1&&prices[i]>=prices[i+1]){
                i++;
            }
            currentMin=prices[i];
            while(i < prices.length-1&&prices[i]<=prices[i+1]){
                i++;
            }
            currentMax=prices[i];
            profit+=currentMax-currentMin;
        }
        return profit;
    }

    //---------------一次遍历法-----------1ms
    /**
     * 低价买进后遇到涨价就卖，可能当天就买进，也可能不买进，可以看官方题解加深印象
     * 在价格不断上升的过程中的涨差价的和等同于最高峰减去低谷的和
     * @param prices
     * @return
     */
    public static int maxProfitTwo(int[] prices) {
        int maxprofit = 0;
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] > prices[i - 1]) {
                maxprofit += prices[i] - prices[i - 1];
            }
        }
        return maxprofit;
    }

    public static void main(String[] args) {
        int[] ints = {1, 7, 2, 3, 6, 7, 6, 7};
        int i = maxProfitTwo(ints);
        System.out.println(i);
    }
}
