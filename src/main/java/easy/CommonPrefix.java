package easy;

/**
 * 描述：求解字符串数组元素的最长公共前缀
 *
 * @author zhanghaoyu
 * @date 2018/12/24 20:30
 **/
public class CommonPrefix {
    /**
     * 最长公共前缀，以第一个字符串为基准不断减小字串长度
     * @param strs 字符串数组
     * @return 最长公共前缀
     */
    public static String longestCommonPrefix(String[] strs) {
        if(strs==null||strs.length==0){
            return "";
        }
        String prefix = strs[0];
        for (int i = 1; i < strs.length; i++) {
            //直到为0的位置才算找到前缀
            while (strs[i].indexOf(prefix)!=0){
                //前缀自减
                prefix = prefix.substring(0, prefix.length() - 1);
                //如果为空说明没有公共的前缀
                if("".equals(prefix)){
                    return "";
                }
            }
        }
        return prefix;
    }

    //-----------------------------------------2---------------------------------------------
    /**
     * 优化版最长公共前缀(分治法策略)
     * 存在的问题:递归操作会比循环要慢(可能会出现提交超时,优先考虑循环)
     * @param strs 字符串数组
     * @return 最长公共前缀
     */
    public static String longestCommonPrefixTwo(String[] strs) {
       if(strs==null||strs.length==0){
           return "";
       }
       return longestCommonPrefixTwo(strs,0,strs.length-1);
    }

    /**
     * 不断一分为2，找到最后找到两边的公共前缀
     * @param strs
     * @param left
     * @param right
     * @return
     */
    public static String longestCommonPrefixTwo(String[] strs,int left,int right){
        //当left大于等于(一般是等于)right时不再进行下一次递归
        if(left>=right){
            return strs[left];
        }else{
            //将数组一分为2进行处理
            int mid=(left+right)/2;
            String leftPrefix = longestCommonPrefixTwo(strs, 0, mid);
            String rightPrifix = longestCommonPrefixTwo(strs, mid + 1, right);
            //比较两个字符串找到当前这两个字符串的最长的前缀，递归返回的过程中不断两两比较最后只剩下一个
            return commonPrefix(leftPrefix,rightPrifix);
        }
    }

    /**
     * 找到两个字符串的公共前缀
     * @param leftPrefix 左字符串
     * @param rightPrefix 右字符串
     * @return 公共前缀
     */
    public static String commonPrefix(String leftPrefix,String rightPrefix){
        //当右字符串的起始位置不是左字符串的前缀
        while (rightPrefix.indexOf(leftPrefix)!=0){
            //自减
            leftPrefix = leftPrefix.substring(0, leftPrefix.length() - 1);
            if("".equals(leftPrefix)){
                return "";
            }
        }
        return leftPrefix;
    }
    //----------------------------------------2----------------------------------------------

    public static void main(String[] args) {
        String[] strings = {"flower","flow","flight"};
        String s = longestCommonPrefixTwo(strings);
        System.out.println(s);
    }
}
