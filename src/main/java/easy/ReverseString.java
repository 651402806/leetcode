package easy;

/**
 * 描述：反转字符串
 * url：https://leetcode-cn.com/problems/reverse-string/
 * @author zhanghaoyu
 * @date 2018/12/27 16:44
 **/
public class ReverseString {
    /**
     * 将一个字符串反转输出
     * @param s 待反转的字符串
     * @return 反转完成的字符串
     */
    public static String reverseString(String s) {
        if(s.length()==0){
            return "";
        }
        char[] chars = s.toCharArray();
        for (int i = 0; i <=(chars.length-1)/2 ; i++) {
            char temp=chars[i];
            chars[i]=chars[chars.length-1-i];
            chars[chars.length-1-i]=temp;
        }
        return String.valueOf(chars);
    }

    public static void main(String[] args) {
        System.out.println(reverseString("helnpo"));
    }
}
