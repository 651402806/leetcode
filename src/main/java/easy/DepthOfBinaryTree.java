package easy;

import javafx.util.Pair;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 描述：给定一个二叉树求解二叉树深度
 * url:https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/submissions/
 * @author zhanghaoyu
 * @date 2018/12/26 15:21
 **/
public class DepthOfBinaryTree {
    /**
     * 树节点结构
     */
    public static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
    }

    //----------------------------递归解法-------------------1ms
    /**
     * 给定一个根节点，求解这个二叉树的深度
     * 时间复杂度：我们每个结点只访问一次，因此时间复杂度为O(n),其中n为节点数量
     * 空间复杂度：在最糟糕的情况下，树是完全不平衡的，例如每个结点只剩下左子结点，递归将会被调用 N 次（树的高度），
     * 因此保持调用栈(方法调用栈)的存储将是O(N)，但在最好的情况下（树是完全平衡的），树的高度将是log(N)
     * 因此，在这种情况下的空间复杂度将是O(log(N))
     * @param root 根节点
     * @return 深度
     */
    public static int maxDepth(TreeNode root) {
        if(root!=null){
            return Math.max(maxDepth(root.left)+1,maxDepth(root.right)+1);
        }else{
            return 0;
        }
    }

    //-----------------------循环迭代解法-------------------14ms
    /**
     * 给定一个根节点，求解这个二叉树的深度
     * 对pair的使用，pair可以构建一个简单的键值对在leetcode上使用时需要手动加包
     * 所以使用pair的时候需要慎重，这里使用pair在每一个节点上保存了其深度层序进行遍历，最后取最大深度值
     * 时间复杂度O(N)
     * 空间复杂度O(N),相比于递归解法，此方法使用了更多的空间，所以较慢
     * @param root 根节点
     * @return 深度
     */
    public static int maxDepthTwo(TreeNode root) {
        Queue<Pair<TreeNode,Integer>> queue = new LinkedList<>();
        queue.add(new Pair<>(root,1));
        int depth=0;
        while(!queue.isEmpty()){
            Pair<TreeNode, Integer> pair = queue.poll();
            TreeNode currentNode = pair.getKey();
            Integer currentDepth = pair.getValue();
            if(currentNode!=null){
                depth = Math.max(depth, currentDepth);
                queue.add(new Pair<>(currentNode.left,currentDepth+1));
                queue.add(new Pair<>(currentNode.right,currentDepth+1));
            }
        }
        return depth;
    }

    public static void main(String[] args) {
        //Given binary tree [3,9,20,null,null,15,7]
        TreeNode treeNode1 = new TreeNode(3);
        TreeNode treeNode2 = new TreeNode(9);
        TreeNode treeNode3 = new TreeNode(20);
        TreeNode treeNode6 = new TreeNode(15);
        TreeNode treeNode7 = new TreeNode(7);
        treeNode1.left=treeNode2;
        treeNode1.right=treeNode3;
        treeNode3.left=treeNode6;
        treeNode3.left=treeNode7;
        System.out.println(maxDepthTwo(treeNode1));
    }
}
